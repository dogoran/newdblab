import com.dao.DbConnection;
import com.dao.Student;

import java.sql.SQLException;

/**
 * Created by dogor on 18.06.2017.
 */
public class Main {
    public static void main(String[] args) throws SQLException {
       //getAll
        com.dao.Student stud = new Student();
        for (com.dto.Student student : stud.getAll()) {
            System.out.println("Student: " + student.getName() + ", Id : " + student.getId());
        }

        //Update
        com.dao.Student stud1 = new Student();
        com.dto.Student studDto = new com.dto.Student(5,"Ivan Ivan");
        if(stud1 != null){
            System.out.println("Updated.");
        }else
        {
            System.out.println("Update Error");
        }
    }

}
