package com.dto;

/**
 * Created by dogor on 18.06.2017.
 */
public class Student {
    private int id;
    private String name;

    public Student(int id,String name){
        this.id = id;
        this.name = name;
    }
    Student(){}

    public void setId(int id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }

    public int getId(){
        return id;
    }
    public String getName(){
        return name;
    }
}
