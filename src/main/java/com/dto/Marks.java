package com.dto;

/**
 * Created by dogor on 18.06.2017.
 */
public class Marks {
    private int id;
    private int mark;

    public Marks(int id, int mark){
        this.id = id;
        this.mark = mark;
    }
    Marks(){}

    public void setId(int id){
        this.id = id;
    }
    public void setMark(int mark){
        this.mark = mark;
    }
    public int getId(){
        return id;
    }
    public int getMark(){
        return mark;
    }
}
