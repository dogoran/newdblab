package com.gproperties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by dogor on 14.06.2017.
 */
public class PropertiesUtil {

    private static String dbUrl;
    private static String dbUser;
    private static String dbPassword;
    private static String dbDriver;

    private static PropertiesUtil instance = null;

    public static PropertiesUtil getInstance(){
        if(instance == null){
            instance = new PropertiesUtil();
        }
        return instance;
    }

    private PropertiesUtil() {
        Properties property = new Properties();

            try {
            FileInputStream fis = new FileInputStream("src/main/resources/config.properties");
            property.load(fis);

            dbUrl = property.getProperty("db.url");
            dbUser = property.getProperty("db.user");
            dbPassword = property.getProperty("db.password");
            dbDriver = property.getProperty("db.password");

        } catch (IOException e) {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }
    }

    public static String getDbUrl() {
        return dbUrl;
    }
    public static String getDbUser() {
        return dbUser;
    }
    public static String getDbPassword() {
        return dbPassword;
    }
    public static String getDbDriver() {
        return dbDriver;
    }
}
