package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dogor on 18.06.2017.
 */
public class Marks implements IDao<com.dto.Marks> {
    private static DbConnection connection = null;

    Marks(){
        connection = DbConnection.getInstance();
    }


    public List<com.dto.Marks> getAll() {
        String query = "select * from marks";
        ResultSet rs = connection.getResultSet();


        try {
            connection.executeQuery(query);
            rs = connection.getResultSet();
            List<com.dto.Marks> marksList = new ArrayList<>();

            while (rs.next()) {
                marksList.add(new com.dto.Marks(rs.getInt("id"),rs.getInt("mark")));
            }
            return marksList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public com.dto.Marks update(com.dto.Marks entity) {

        String query = "update marks "+ "SET mark = " + entity.getMark()
                                        + " where id = " + entity.getId();
        try {
            connection.executeUpdate(query);
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public com.dto.Marks getEntityById(int id) {
        String query = "select * from marks where id = " + id;
        ResultSet rs = connection.getResultSet();

        try {
            connection.executeQuery(query);
            rs = connection.getResultSet();
            return new com.dto.Marks(rs.getInt("id"),
                                       rs.getInt("mark"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(int id) {
        String query = "delete marks where id = " + id;
        try {
            connection.executeUpdate(query);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean insert(com.dto.Marks entity) {
        String query = "INSERT INTO marks"
                        + "(mark) " + "VALUES"
                        + "('"+entity.getMark()+"')";
        try {
            connection.executeUpdate(query);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
