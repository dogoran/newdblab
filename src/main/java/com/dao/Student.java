package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dogor on 18.06.2017.
 */
public class Student implements IDao<com.dto.Student> {
    private static DbConnection connection = null;

    public Student(){
        connection = DbConnection.getInstance();
    }


    public List<com.dto.Student> getAll() {
        String query = "select * from student";
        ResultSet rs = connection.getResultSet();


        try {
            connection.executeQuery(query);
            rs = connection.getResultSet();
            List<com.dto.Student> studList = new ArrayList<>();

            while (rs.next()) {
                studList.add(new com.dto.Student(rs.getInt("id"),rs.getString("name")));
            }
            return studList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public com.dto.Student update(com.dto.Student entity) {

        String query = "update student "+ "SET name = " + entity.getName()
                                        + " where id = " + entity.getId();
        try {
            connection.executeUpdate(query);
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public com.dto.Student getEntityById(int id) {
        String query = "select * from student where id = " + id;
        ResultSet rs = connection.getResultSet();

        try {
            connection.executeQuery(query);
            rs = connection.getResultSet();
            return new com.dto.Student(rs.getInt("id"),
                                       rs.getString("name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(int id) {
        String query = "delete student where id = " + id;
        try {
            connection.executeUpdate(query);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean insert(com.dto.Student entity) {
        String query = "INSERT INTO student"
                        + "(name) " + "VALUES"
                        + "('"+entity.getName()+"')";
        try {
            connection.executeUpdate(query);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
