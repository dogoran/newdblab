package com.dao;

import com.gproperties.PropertiesUtil;
import com.sun.org.apache.regexp.internal.RE;

import java.sql.*;


/**
 * Created by dogor on 08.05.2017.
 */
public class DbConnection {

    private Connection dbConnection = null;
    private static DbConnection instance = null;
    private ResultSet rs;

    public static DbConnection getInstance(){
        if(instance == null){
            instance = new DbConnection();
        }
        return instance;
    }

    private Connection DbConnection() {
        PropertiesUtil properties = PropertiesUtil.getInstance();
        try {
            Class.forName(properties.getDbDriver());
        } catch (ClassNotFoundException e) {
            System.out.println("Where`s ur db driver? :C");
            System.out.println(e.getMessage());
            return dbConnection;
        }

        try {
            dbConnection = DriverManager.getConnection(properties.getDbUrl(),
                            properties.getDbUser(),properties.getDbPassword());
            return dbConnection;
        } catch (SQLException e) {
            System.out.println("Mb u forgot ur pswrd?))");
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }

    public Connection getConnection() throws SQLException {
        return dbConnection;
    }
    public ResultSet getResultSet(){
        return rs;
    }

    public void executeQuery(String query) throws SQLException {
        Statement statement = null;
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(query);
        }catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void executeUpdate(String query) throws SQLException {
        Statement statement = null;
        try {
            statement = dbConnection.createStatement();
            statement.execute(query);
        }catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
