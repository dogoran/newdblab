package com.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by dogor on 11.06.2017.
 */
public interface IDao<E> {

    public abstract List<E> getAll();
    public abstract E update(E entity);
    public abstract E getEntityById(int id);
    public abstract boolean delete(int id);
    public abstract boolean insert(E entity);

}
